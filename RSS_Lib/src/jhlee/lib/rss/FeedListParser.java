package jhlee.lib.rss;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class FeedListParser extends FeedParserBase {

	private Map<String, Indicator> indicators;

	public FeedListParser(String titleIndicator, int frontSkipCount,
			int middleSkipCount, int rearSkipCount) {
		indicators = new Hashtable<String, Indicator>();
		setTitleIndicator(titleIndicator, frontSkipCount, middleSkipCount,
				rearSkipCount);
	}

	public void setTitleIndicator(String indicator, int frontSkipCount,
			int middleSkipCount, int rearSkipCount) {
		indicators.put(TAG_TITLE, new Indicator(indicator, frontSkipCount,
				middleSkipCount, rearSkipCount));
	}

	public void setLinkIndicator(String indicator, int frontSkipCount,
			int middleSkipCount, int rearSkipCount) {
		indicators.put(TAG_LINK, new Indicator(indicator, frontSkipCount,
				middleSkipCount, rearSkipCount));
	}

	public void addDescriptionIndicator(String indicator, int frontSkipCount,
			int middleSkipCount, int rearSkipCount) {
		indicators.put(TAG_DESCRIPTION, new Indicator(indicator,
				frontSkipCount, middleSkipCount, rearSkipCount));
	}

	public void addPubDateIndicator(String indicator, int frontSkipCount,
			int middleSkipCount, int rearSkipCount) {
		indicators.put(TAG_PUB_DATE, new Indicator(indicator, frontSkipCount,
				middleSkipCount, rearSkipCount));
	}

	public void addAuthorIndicator(String indicator, int frontSkipCount,
			int middleSkipCount, int rearSkipCount) {
		indicators.put(TAG_AUTOR, new Indicator(indicator, frontSkipCount,
				middleSkipCount, rearSkipCount));
	}

	public Feed getFeed(String url) {
		List<Feed> feeds = getFeeds(url, 1);

		if (feeds.size() > 0) {
			return feeds.get(0);
		} else {
			return null;
		}
	}

	public List<Feed> getFeeds(String url) {
		return getFeeds(url, 0);
	}

	public List<Feed> getFeeds(String url, int maxSize) {
		List<Feed> result = new ArrayList<>();

		try {
			org.jsoup.nodes.Document doc = Jsoup.connect(url).timeout(15000)
					.get();

			Indicator titleIndicator = indicators.get(TAG_TITLE);
			Indicator linkIndicator = indicators.get(TAG_LINK);
			Indicator descriptionIndicator = indicators.get(TAG_DESCRIPTION);
			Indicator pubDateIndicator = indicators.get(TAG_PUB_DATE);
			Indicator authorIndicator = indicators.get(TAG_AUTOR);

			Elements titles = null;
			Elements links = null;
			Elements descriptions = null;
			Elements authors = null;
			Elements pubDates = null;

			titles = doc.select(titleIndicator.indicator);
			if (linkIndicator != null) {
				links = doc.select(linkIndicator.indicator);
			}
			if (descriptionIndicator != null) {
				descriptions = doc.select(descriptionIndicator.indicator);
			}
			if (pubDateIndicator != null) {
				pubDates = doc.select(pubDateIndicator.indicator);
			}
			if (authorIndicator != null) {
				authors = doc.select(authorIndicator.indicator);
			}

			int size = titles == null ? 0
					: (titles.size() - titleIndicator.frontSkipCount
							- titleIndicator.rearSkipCount + titleIndicator.middleSkipCount)
							/ (titleIndicator.middleSkipCount + 1);
			if (maxSize != 0) {
				size = size > maxSize ? maxSize : size;
			}
			for (int i = 0; i < size; i++) {
				Feed feed = new Feed();

				feed.title = titles.get(
						i * (titleIndicator.middleSkipCount + 1)
								+ titleIndicator.frontSkipCount).text();
				if (links != null) {
					try {
						Element e = links.get(i
								* (linkIndicator.middleSkipCount + 1)
								+ linkIndicator.frontSkipCount);
						feed.link = e.attr("href");
						if (feed.link == null || feed.link.equals("")) {
							feed.link = e.text();
						}
						if (feed.link == null) {
							feed.link = url;
						}
						if (feed.link != null && feed.link.startsWith("/")) {
							URL u = new URL(url);
							feed.link = u.getProtocol() + "://" + u.getHost()
									+ feed.link;
						}
					} catch (Exception ignore) {
					}
				}
				if (descriptionIndicator != null) {
					try {
						feed.description = descriptions.get(
								i * (descriptionIndicator.middleSkipCount + 1)
										+ descriptionIndicator.frontSkipCount)
								.text();
					} catch (Exception ignore) {
					}
				}
				if (pubDateIndicator != null) {
					try {
						feed.pubDate = pubDates.get(
								i * (pubDateIndicator.middleSkipCount + 1)
										+ pubDateIndicator.frontSkipCount)
								.text();
					} catch (Exception ignore) {
					}
				}
				if (authorIndicator != null) {
					try {
						feed.author = authors.get(
								i * (authorIndicator.middleSkipCount + 1)
										+ authorIndicator.frontSkipCount)
								.text();
					} catch (Exception ignore) {
					}
				}

				result.add(feed);
			}
		} catch (IOException e) {
		}

		return result;
	}
}
