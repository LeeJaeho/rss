package jhlee.lib.rss;

public class Feed {

	public String title;
	public String description;
	public String author;
	public String pubDate;
	public String link;
}
