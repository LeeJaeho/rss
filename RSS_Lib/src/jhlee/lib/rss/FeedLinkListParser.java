package jhlee.lib.rss;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class FeedLinkListParser extends FeedParserBase {

	private Map<String, Indicator> indicators;

	private LinkConverter linkConverter;

	public FeedLinkListParser(String linkIndicator, int linkFrontSkipCount,
			int linkMiddleSkipCount, int linkRearSkipCount,
			String titleIndicator, int titleSkipCount) {
		indicators = new Hashtable<String, Indicator>();
		setLinkIndicator(linkIndicator, linkFrontSkipCount,
				linkMiddleSkipCount, linkRearSkipCount);
		setTitleIndicator(titleIndicator, titleSkipCount);
	}

	public void setLinkIndicator(String indicator, int frontSkipCount,
			int middleSkipCount, int rearSkipCount) {
		indicators.put(TAG_LINK, new Indicator(indicator, frontSkipCount,
				middleSkipCount, rearSkipCount));
	}

	public void setTitleIndicator(String indicator, int skipCount) {
		indicators.put(TAG_TITLE, new Indicator(indicator, skipCount, 0, 0));
	}

	public void addDescriptionIndicator(String indicator, int skipCount) {
		indicators.put(TAG_DESCRIPTION, new Indicator(indicator, skipCount, 0,
				0));
	}

	public void addPubDateIndicator(String indicator, int skipCount) {
		indicators.put(TAG_PUB_DATE, new Indicator(indicator, skipCount, 0, 0));
	}

	public void addAuthorIndicator(String indicator, int skipCount) {
		indicators.put(TAG_AUTOR, new Indicator(indicator, skipCount, 0, 0));
	}

	public void setLinkConverter(LinkConverter adapter) {
		linkConverter = adapter;
	}

	private String convertLink(String link) {
		if (linkConverter != null) {
			return linkConverter.convertLink(link);
		} else {
			return link;
		}
	}

	public Feed getFeed(String url) {
		List<Feed> feeds = getFeeds(url, 1);

		if (feeds.size() > 0) {
			return feeds.get(0);
		} else {
			return null;
		}
	}

	public List<Feed> getFeeds(String url) {
		return getFeeds(url, 0);
	}

	public List<Feed> getFeeds(String url, int maxSize) {
		List<Feed> result = new ArrayList<>();

		try {
			org.jsoup.nodes.Document doc = Jsoup.connect(url).timeout(15000)
					.get();

			Indicator titleIndicator = indicators.get(TAG_TITLE);
			Indicator linkIndicator = indicators.get(TAG_LINK);
			Indicator descriptionIndicator = indicators.get(TAG_DESCRIPTION);
			Indicator pubDateIndicator = indicators.get(TAG_PUB_DATE);
			Indicator authorIndicator = indicators.get(TAG_AUTOR);

			Elements links = null;

			if (linkIndicator != null && !linkIndicator.equals("")) {
				links = doc.select(linkIndicator.indicator);
			}

			int size = links == null ? 0
					: (links.size() - linkIndicator.frontSkipCount
							- linkIndicator.rearSkipCount + linkIndicator.middleSkipCount)
							/ (linkIndicator.middleSkipCount + 1);
			if (maxSize != 0) {
				size = size > maxSize ? maxSize : size;
			}
			for (int i = 0; i < size; i++) {
				Feed feed = new Feed();

				try {
					Element e = links.get(i
							* (linkIndicator.middleSkipCount + 1)
							+ linkIndicator.frontSkipCount);
					feed.link = e.attr("href");
					if (feed.link == null || feed.link.equals("")) {
						feed.link = e.text();
					}
					if (feed.link == null) {
						feed.link = url;
					}
					if (feed.link != null && feed.link.startsWith("/")) {
						URL u = new URL(url);
						feed.link = u.getProtocol() + "://" + u.getHost()
								+ feed.link;
					}
					feed.link = convertLink(feed.link);
				} catch (Exception ignore) {
				}

				org.jsoup.nodes.Document doc2 = Jsoup.connect(feed.link)
						.timeout(15000).get();

				try {
					feed.title = doc2.select(titleIndicator.indicator)
							.get(titleIndicator.frontSkipCount).text();
				} catch (Exception ignore) {
				}
				if (descriptionIndicator != null) {
					try {
						feed.description = doc2
								.select(descriptionIndicator.indicator)
								.get(descriptionIndicator.frontSkipCount)
								.text();
					} catch (Exception ignore) {
					}
				}
				if (pubDateIndicator != null) {
					try {
						feed.pubDate = doc2.select(pubDateIndicator.indicator)
								.get(pubDateIndicator.frontSkipCount).text();
					} catch (Exception ignore) {
					}
				}
				if (authorIndicator != null) {
					try {
						feed.author = doc2.select(authorIndicator.indicator)
								.get(authorIndicator.frontSkipCount).text();
					} catch (Exception ignore) {
					}
				}

				result.add(feed);
			}
		} catch (IOException e) {
		}

		return result;
	}

	public interface LinkConverter {
		public String convertLink(String link);
	}
}
