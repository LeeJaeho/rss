package jhlee.lib.rss;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;

public class FeedParser extends FeedParserBase {

	private Map<String, Indicator> indicators;

	public FeedParser(String titleIndicator, int skipCount) {
		indicators = new Hashtable<String, Indicator>();
		setTitleIndicator(titleIndicator, skipCount);
	}

	public void setTitleIndicator(String indicator, int skipCount) {
		indicators.put(TAG_TITLE, new Indicator(indicator, skipCount, 0, 0));
	}

	public void addDescriptionIndicator(String indicator, int skipCount) {
		indicators.put(TAG_DESCRIPTION, new Indicator(indicator, skipCount, 0,
				0));
	}

	public void addPubDateIndicator(String indicator, int skipCount) {
		indicators.put(TAG_PUB_DATE, new Indicator(indicator, skipCount, 0, 0));
	}

	public void addAuthorIndicator(String indicator, int skipCount) {
		indicators.put(TAG_AUTOR, new Indicator(indicator, skipCount, 0, 0));
	}

	public Feed getFeed(String url) {
		try {
			org.jsoup.nodes.Document doc = Jsoup.connect(url).timeout(15000)
					.get();

			Indicator titleIndicator = indicators.get(TAG_TITLE);
			Indicator descriptionIndicator = indicators.get(TAG_DESCRIPTION);
			Indicator pubDateIndicator = indicators.get(TAG_PUB_DATE);
			Indicator authorIndicator = indicators.get(TAG_AUTOR);

			Feed feed = new Feed();

			try {
				feed.title = doc.select(titleIndicator.indicator)
						.get(titleIndicator.frontSkipCount).text();
			} catch (Exception e) {

			}
			feed.link = url;

			if (descriptionIndicator != null) {
				try {
					feed.description = doc
							.select(descriptionIndicator.indicator)
							.get(descriptionIndicator.frontSkipCount).text();
				} catch (Exception ignore) {
				}
			}
			if (pubDateIndicator != null) {
				try {
					feed.pubDate = doc.select(pubDateIndicator.indicator)
							.get(pubDateIndicator.frontSkipCount).text();
				} catch (Exception ignore) {
				}
			}
			if (authorIndicator != null) {
				try {
					feed.author = doc.select(authorIndicator.indicator)
							.get(authorIndicator.frontSkipCount).text();
				} catch (Exception ignore) {
				}
			}

			return feed;
		} catch (IOException e) {
		}

		return null;
	}

	public List<Feed> getFeeds(String url) {
		return getFeeds(url, 0);
	}

	public List<Feed> getFeeds(String url, int maxSize) {
		List<Feed> result = new ArrayList<>();

		Feed feed = getFeed(url);

		if (feed != null) {
			result.add(feed);
		}

		return result;
	}
}
