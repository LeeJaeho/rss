package jhlee.lib.rss;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public abstract class FeedParserBase {

	static final String TAG_TITLE = "title";
	static final String TAG_DESCRIPTION = "description";
	static final String TAG_AUTOR = "autor";
	static final String TAG_PUB_DATE = "pubDate";
	static final String TAG_LINK = "link";

	public abstract Feed getFeed(String url);

	public abstract List<Feed> getFeeds(String url);

	public abstract List<Feed> getFeeds(String url, int maxSize);

	public String feedToRss(Feed feed) {
		if (feed == null) {
			return null;
		}

		List<Feed> feeds = new ArrayList<Feed>();

		feeds.add(feed);

		return feedToRss(feeds);
	}

	public String feedToRss(List<Feed> feeds) {
		if (feeds.size() == 0) {
			return null;
		}

		String firstItemLink = feeds.get(0).link;
		String link = null;

		if (firstItemLink != null) {

		}

		if (firstItemLink != null) {
			try {
				URL u = new URL(firstItemLink);
				link = u.getProtocol() + "://" + u.getHost();
			} catch (Exception ignore) {
			}
		}

		String result = "";
		result += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
				+ "<rss version=\"2.0\" xmlns:content=\"http://purl.org/rss/1.0/modules/content/\">"
				+ "<channel>";

		if (link != null) {
			result += "<link>" + "<![CDATA[" + link + "]]>" + "</link>";
		}
		result += "<pubDate>" + new Date() + "</pubDate>";

		for (Feed feed : feeds) {
			result += "<item>";

			if (feed.author != null) {
				result += "<author><![CDATA[" + feed.author + "]]></author>";
			}
			if (feed.title != null) {
				result += "<title><![CDATA[" + feed.title + "]]></title>";
			}
			if (feed.description != null) {
				result += "<description><![CDATA[" + feed.description
						+ "]]></description>";
			}
			if (feed.link != null) {
				result += "<link><![CDATA[" + feed.link + "]]></link>";
			}
			if (feed.pubDate != null) {
				result += "<pubDate>" + feed.pubDate + "</pubDate>";
			}
			result += "</item>";
		}
		result += "</channel>" + "</rss>";

		return result;
	}

	class Indicator {
		public String indicator;
		public int frontSkipCount;
		public int middleSkipCount;
		public int rearSkipCount;

		public Indicator(String indicator, int frontSkipCount,
				int middleSkipCount, int rearSkipCount) {
			this.indicator = indicator;
			this.frontSkipCount = frontSkipCount;
			this.middleSkipCount = middleSkipCount;
			this.rearSkipCount = rearSkipCount;
		}
	}
}
