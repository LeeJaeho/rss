package jhlee.example.rss;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.List;

import jhlee.lib.rss.Feed;
import jhlee.lib.rss.FeedLinkListParser;
import jhlee.lib.rss.FeedLinkListParser.LinkConverter;
import jhlee.lib.rss.FeedListParser;
import jhlee.lib.rss.FeedParser;

public class Main {

	public static void main(String[] args) {
		testFeedLinkList2();
	}

	private static void testFeed() {
		FeedParser getter = new FeedParser(".title", 0);

		getter.addAuthorIndicator(".author", 0);
		getter.addPubDateIndicator("span[class=label label-info]", 0);
		getter.addDescriptionIndicator(".article", 0);

		Feed feed = getter.getFeed("http://onasaju.tistory.com/316");

		System.out.println(feed.title);
		System.out.println(feed.link);
		System.out.println(feed.author);
		System.out.println(feed.pubDate);
		System.out.println(feed.description);
	}

	private static void testFeed2() {
		FeedParser getter = new FeedParser("[class=bdrNone textAL]", 1);

		getter.addAuthorIndicator(".noBd", 0);
		getter.addPubDateIndicator("[class=textAL]", 1);
		getter.addDescriptionIndicator("[class=bdViewCont cf]", 0);

		Feed feed = getter
				.getFeed("http://www.inu.ac.kr/user/boardList.do?command=view&page=1&boardId=48510&boardSeq=362109&id=inu_070201000000");

		System.out.println(feed.title);
		System.out.println(feed.link);
		System.out.println(feed.author);
		System.out.println(feed.pubDate);
		System.out.println(feed.description);
	}

	private static void testFeedList() {
		FeedListParser getter = new FeedListParser(".msnry_title", 0, 0, 0);

		getter.addPubDateIndicator("i[class=fa fa-clock-o]", 0, 0, 0);
		getter.setLinkIndicator(".msnry_title a", 0, 0, 0);

		List<Feed> feeds = getter
				.getFeeds("http://onasaju.tistory.com/category");

		for (Feed f : feeds) {
			System.out.println(f.title);
			System.out.println(f.link);
			System.out.println(f.pubDate);
		}
	}

	private static void testFeedLinkList() {
		FeedLinkListParser getter = new FeedLinkListParser(
				"[class=msnry_title] a", 0, 0, 0, ".title", 0);

		getter.addAuthorIndicator(".author", 0);
		getter.addPubDateIndicator("span[class=label label-info]", 0);
		getter.addDescriptionIndicator(".article", 0);

		List<Feed> feeds = getter.getFeeds(
				"http://onasaju.tistory.com/category", 2);

		for (Feed f : feeds) {
			System.out.println(f.title);
			System.out.println(f.link);
			System.out.println(f.author);
			System.out.println(f.pubDate);
			System.out.println(f.description);
		}
	}

	private static void testFeedLinkList2() {
		FeedLinkListParser getter = new FeedLinkListParser(".textAL a", 0, 0,
				0, "[class=bdrNone textAL]", 1);

		getter.setLinkConverter(new LinkConverter() {

			@Override
			public String convertLink(String link) {
				return "http://www.inu.ac.kr/user/" + link;
			}
		});

		getter.addAuthorIndicator(".noBd", 0);
		getter.addPubDateIndicator("[class=textAL]", 1);
		getter.addDescriptionIndicator("[class=bdViewCont cf]", 0);

		List<Feed> feeds = getter
				.getFeeds(
						"http://www.inu.ac.kr/user/boardList.do?boardId=48510&siteId=inu&id=inu_070201000000",
						3);

		try {
			File f = new File("asdf.xml");
			OutputStream os = new FileOutputStream(f, false);
			OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");

			osw.write(getter.feedToRss(feeds));
			osw.flush();

			osw.close();
		} catch (IOException e) {
		}
	}
}
